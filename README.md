# What is a Python developer?

This document is intended to describe a model for a Python
developer as an RSE.
What skills are they expected to have?
It can be used a template against which to mark-up your
professional development.


Can program in Python:
  - types
  - functions
  - if / elif / for
  - list vs sequence vs generator/iterator
  - list vs tuple
  - tuple unpacking
  - objects / classes
  - use builtin modules
    - functools
    - itertools
    - argparse
  - make a new module
  - file io, using with
  - try, except
  - strings
  - format strings
  - making a command line tool (see argparse)
  - make a webserver? (easy in flask, painful using HTTPserver)
  - integrate a C/C++ library


Ecosystem
  - use a 3rd party module/package
  - install 3rd party module from non-standard place (download)
  - can make a package and upload to PyPI
  - assess a 3rd party package for suitability (or indeed a 1st
    or 2nd party package)


Tooling
  - can use github
  - can use git
  - IDLE / Spyder / PyCharm / other IDE?


Data Processing
  - JSON
  - CSV
  - HDF5 / pickle


Visualisation
  - matplotlib
  - other things?


Application Development
  - Django
  - flask
  - Tcl/tk

